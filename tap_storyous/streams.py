"""Stream type classes for tap-storyous."""

from typing import Optional

from singer_sdk import typing as th

from tap_storyous.client import StoryousStream


class MerchantStream(StoryousStream):
    """Define custom stream."""

    name = "merchants"

    @property
    def path(self):
        return f"/merchants/{self.config.get('merchant_id')}"

    primary_keys = ["merchantId"]
    replication_key = None
    records_jsonpath = "$"

    schema = th.PropertiesList(
        th.Property("merchantId", th.StringType),
        th.Property("name", th.StringType),
        th.Property("businessId", th.StringType),
        th.Property("vatId", th.StringType),
        th.Property("isVatPayer", th.BooleanType),
        th.Property("countryCode", th.StringType),
        th.Property("currencyCode", th.StringType),
        th.Property(
            "places",
            th.ArrayType(
                th.ObjectType(
                    th.Property("placeId", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("state", th.StringType),
                    th.Property("phoneNumber", th.StringType),
                    th.Property(
                        "addressParts",
                        th.ObjectType(
                            th.Property("street", th.StringType),
                            th.Property("streetNumber", th.StringType),
                            th.Property("city", th.StringType),
                            th.Property("country", th.StringType),
                            th.Property("countryCode", th.StringType),
                            th.Property("zip", th.StringType),
                            th.Property("latitude", th.CustomType({"type": ["number", "string"]})),
                            th.Property("longitude", th.CustomType({"type": ["number", "string"]})),
                        ),
                    ),
                )
            ),
        ),
    ).to_dict()


class BillsStream(StoryousStream):
    """Define custom stream."""

    name = "bills"

    @property
    def path(self):
        return f"/bills/{self.source_id}"

    primary_keys = ["billId"]
    replication_key = "_lastModifiedAt"
    rep_key_param = "modifiedSince"
    pagination_param = "lastBillId"
    schema = th.PropertiesList(
        th.Property("billId", th.StringType),
        th.Property("sessionCreated", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("paidAt", th.DateTimeType),
        th.Property("fiscalizedAt", th.DateTimeType),
        th.Property("finalPrice", th.NumberType),
        th.Property("finalPriceWithoutTax", th.StringType),
        th.Property("taxSummaries", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "taxes",
            th.ArrayType(
                th.ObjectType(
                    th.Property("vat", th.StringType),
                    th.Property("totalVat", th.StringType),
                    th.Property("totalWithoutVat", th.StringType),
                )
            ),
        ),
        th.Property("discount", th.NumberType),
        th.Property("rounding", th.NumberType),
        th.Property("tips", th.NumberType),
        th.Property("currencyCode", th.StringType),
        th.Property("refunded", th.BooleanType),
        th.Property("refundedBillIdentifier", th.StringType),
        th.Property("paymentMethod", th.StringType),
        th.Property(
            "payments",
            th.ArrayType(
                th.ObjectType(
                    th.Property("paymentMethod", th.StringType),
                    th.Property("priceWithVat", th.StringType),
                )
            ),
        ),
        th.Property(
            "createdBy",
            th.ObjectType(
                th.Property("personId", th.IntegerType),
                th.Property("fullName", th.StringType),
                th.Property("userName", th.StringType),
            ),
        ),
        th.Property(
            "paidBy",
            th.ObjectType(
                th.Property("personId", th.IntegerType),
                th.Property("fullName", th.StringType),
                th.Property("userName", th.StringType),
            ),
        ),
        th.Property("personCount", th.IntegerType),
        th.Property("deskId", th.StringType),
        th.Property("issuedAsVatPayer", th.BooleanType),
        th.Property(
            "fiscalData",
            th.ObjectType(
                th.Property("fik", th.StringType),
                th.Property("pkp", th.StringType),
                th.Property("httpStatusCode", th.IntegerType),
                th.Property("endpoint", th.StringType),
                th.Property("mode", th.IntegerType),
                th.Property("bkp", th.StringType),
            ),
        ),
        th.Property("invoiceData", th.CustomType({"type": ["object", "string"]})),
        th.Property("customerId", th.StringType),
        th.Property("orderProvider", th.CustomType({"type": ["object", "string"]})),
        th.Property("_lastModifiedAt", th.DateTimeType),
    ).to_dict()


class MenuStream(StoryousStream):
    """Define custom stream."""

    name = "menu"

    @property
    def path(self):
        return f"/menu/{self.config.get('merchant_id')}"

    primary_keys = ["menuId"]
    records_jsonpath = "$"

    schema = th.PropertiesList(
        th.Property("menuId", th.StringType),
        th.Property(
            "items",
            th.ArrayType(
                th.ObjectType(
                    th.Property("categoryId", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("posName", th.StringType),
                    th.Property("color", th.StringType),
                    th.Property("icon", th.StringType),
                    th.Property(
                        "items",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("categoryId", th.StringType),
                                th.Property("name", th.StringType),
                                th.Property("posName", th.StringType),
                                th.Property("color", th.StringType),
                                th.Property("icon", th.StringType),
                                th.Property(
                                    "items",
                                    th.ArrayType(
                                        th.ObjectType(
                                            th.Property("productId", th.StringType),
                                            th.Property("decodedId", th.IntegerType),
                                            th.Property("type", th.StringType),
                                            th.Property("name", th.StringType),
                                            th.Property("marketingName", th.StringType),
                                            th.Property("ean", th.StringType),
                                            th.Property("measure", th.StringType),
                                            th.Property(
                                                "isPriceVariable", th.BooleanType
                                            ),
                                            th.Property("description", th.StringType),
                                            th.Property("imageUrl", th.StringType),
                                            th.Property(
                                                "labels",
                                                th.ArrayType(
                                                    th.CustomType(
                                                        {"type": ["object", "string"]}
                                                    )
                                                ),
                                            ),
                                            th.Property(
                                                "placesValues",
                                                th.CustomType(
                                                    {"type": ["object", "string"]}
                                                ),
                                            ),
                                        )
                                    ),
                                ),
                            )
                        ),
                    ),
                ),
            ),
        ),
        th.Property(
            "virtualProducts",
            th.ArrayType(
                th.ObjectType(
                    th.Property("productId", th.StringType),
                    th.Property("type", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("measure", th.StringType),
                    th.Property("vatRate", th.NumberType),
                    th.Property("vatId", th.IntegerType),
                    th.Property("takeawayVatRate", th.NumberType),
                    th.Property("takeawayVatId", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "additionCategories",
            th.ArrayType(
                th.ObjectType(
                    th.Property("additionCategoryId", th.StringType),
                    th.Property("placeId", th.StringType),
                    th.Property("title", th.StringType),
                    th.Property("min", th.IntegerType),
                    th.Property("max", th.IntegerType),
                    th.Property("showInPos", th.BooleanType),
                    th.Property(
                        "integrationData",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("integrationId", th.StringType),
                                th.Property("enabled", th.BooleanType),
                                th.Property("externalId", th.StringType),
                            )
                        ),
                    ),
                    th.Property(
                        "additions",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("additionId", th.StringType),
                                th.Property("productId", th.StringType),
                                th.Property("decodedId", th.IntegerType),
                                th.Property("title", th.StringType),
                                th.Property("vatRate", th.NumberType),
                                th.Property("vatId", th.IntegerType),
                                th.Property("takeawayVatRate", th.NumberType),
                                th.Property("takeawayVatId", th.IntegerType),
                                th.Property("measure", th.StringType),
                                th.Property("additionPrice", th.NumberType),
                                th.Property("subtractionPrice", th.NumberType),
                            )
                        ),
                    ),
                )
            ),
        ),
        th.Property("currencyCode", th.StringType),
        th.Property("_version", th.IntegerType),
        th.Property("_lastModifiedAt", th.DateTimeType),
    ).to_dict()


class StockStream(StoryousStream):
    """Define custom stream."""

    name = "stocks"

    @property
    def path(self):
        return f"/stocks/{self.config.get('merchant_id')}/stocks"

    primary_keys = ["stockId"]

    schema = th.PropertiesList(
        th.Property("stockId", th.StringType),
        th.Property("name", th.StringType),
        th.Property("isCentral", th.BooleanType),
        th.Property("placeId", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "stock_id": record["stockId"],
        }


class StockUpsStream(StoryousStream):
    """Define custom stream."""

    name = "stock_ups"

    @property
    def path(self):
        path = f"/stocks/{self.config.get('merchant_id')}/stocks"
        return path + "/{stock_id}/stockUps"

    primary_keys = ["stockUpId"]
    parent_stream_type = StockStream
    pagination_param = "lastId"

    schema = th.PropertiesList(
        th.Property("stockUpId", th.StringType),
        th.Property("stock_id", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("note", th.StringType),
        th.Property("number", th.StringType),
        th.Property("supplierName", th.StringType),
        th.Property("supplierId", th.StringType),
        th.Property("personId", th.StringType),
        th.Property("personName", th.StringType),
        th.Property("totalPriceWithoutVat", th.NumberType),
        th.Property("isDraft", th.BooleanType),
    ).to_dict()


class ItemsStockStream(StoryousStream):
    """Define custom stream."""

    name = "items_stock"

    @property
    def path(self):
        path = f"/stocks/{self.config.get('merchant_id')}/stocks"
        return path + "/{stock_id}/items"

    primary_keys = ["itemId"]
    parent_stream_type = StockStream
    pagination_param = "lastId"

    schema = th.PropertiesList(
        th.Property("itemId", th.StringType),
        th.Property("stock_id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("categoryName", th.StringType),
        th.Property("measure", th.StringType),
        th.Property("measureId", th.IntegerType),
        th.Property("ean", th.StringType),
        th.Property("vatRate", th.NumberType),
        th.Property("priceWithoutVat", th.NumberType),
        th.Property("priceWithVat", th.NumberType),
        th.Property("amount", th.NumberType),
        th.Property("priceAndAmountUpToDate", th.BooleanType),
        th.Property(
            "measureConversions",
            th.ArrayType(
                th.ObjectType(
                    th.Property("measureId", th.IntegerType),
                    th.Property("coefficient", th.NumberType),
                )
            ),
        ),
    ).to_dict()
