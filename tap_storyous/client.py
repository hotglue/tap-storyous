"""REST client handling, including StoryousStream base class."""

import datetime
import re
from typing import Any, Dict, Optional

import requests
from pendulum import parse
from singer_sdk.streams import RESTStream

from tap_storyous.auth import StoryousAuthenticator


class StoryousStream(RESTStream):
    """Storyous stream class."""

    url_base = "https://api.storyous.com"
    records_jsonpath = "$.data[*]"
    next_page_token_jsonpath = "$.next_page"
    rep_key_param = None
    pagination_param = None

    @property
    def authenticator(self) -> StoryousAuthenticator:
        oauth_url = self.config.get(
            "auth_url", "https://login.storyous.com/api/auth/authorize"
        )
        return StoryousAuthenticator(self, self.config, auth_endpoint=oauth_url)

    @property
    def source_id(self):
        return f"{self.config.get('merchant_id')}-{self.config.get('place_id')}"

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        response = response.json()
        if response.get("nextPage"):
            next_page_token = re.findall(
                f"{self.pagination_param}=(.*)", response.get("nextPage")
            )[0]
            return next_page_token
        return None

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params[self.pagination_param] = next_page_token
        start_date = self.get_starting_time(context)
        if self.replication_key and start_date:
            start_date = start_date + datetime.timedelta(seconds=1)
            start_date = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
            params[self.rep_key_param] = start_date
        return params
