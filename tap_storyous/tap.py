"""Storyous tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_storyous.streams import (
    BillsStream,
    ItemsStockStream,
    MenuStream,
    MerchantStream,
    StockStream,
    StockUpsStream,
)

STREAM_TYPES = [
    MerchantStream,
    BillsStream,
    MenuStream,
    StockStream,
    StockUpsStream,
    ItemsStockStream,
]


class TapStoryous(Tap):
    """Storyous tap class."""

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        super().__init__(config, catalog, state, parse_env_config, validate_config)
        self.config_file = config[0]

    name = "tap-storyous"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
        ),
        th.Property(
            "merchant_id",
            th.StringType,
            required=True,
        ),
        th.Property("place_id", th.StringType, required=True),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapStoryous.cli()
