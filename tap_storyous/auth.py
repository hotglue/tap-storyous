import json
from datetime import datetime
from typing import Optional

import requests
from singer_sdk.authenticators import APIAuthenticatorBase
from singer_sdk.streams import Stream as RESTStreamBase


class StoryousAuthenticator(APIAuthenticatorBase):
    """API Authenticator for OAuth 2.0 flows."""

    def __init__(
        self,
        stream: RESTStreamBase,
        config_file: Optional[str] = None,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        super().__init__(stream=stream)
        self._auth_endpoint = auth_endpoint
        self._config_file = config_file
        self._tap = stream._tap

    @property
    def auth_headers(self) -> dict:
        if not self.is_token_valid():
            self.update_access_token()
        result = super().auth_headers
        result["Authorization"] = f"Bearer {self._tap._config.get('access_token')}"
        return result

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the hubspot API."""
        return {
            "grant_type": "client_credentials",
            "client_id": self._tap._config["client_id"],
            "client_secret": self._tap._config["client_secret"],
        }

    def calculate_expires_in(self, expires_at):
        given_datetime = datetime.strptime(expires_at, "%Y-%m-%dT%H:%M:%S.%fZ")
        current_datetime = datetime.utcnow()
        time_difference = given_datetime - current_datetime
        seconds_left = int(time_difference.total_seconds())
        return seconds_left

    def is_token_valid(self) -> bool:
        access_token = self._tap._config.get("access_token")
        expires_at = self._tap.config.get("expires_at")
        if not access_token:
            return False
        if not expires_at:
            return False
        token_seconds_left = self.calculate_expires_in(expires_at)
        return not (token_seconds_left < 120)

    def update_access_token(self) -> None:
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        token_response = requests.post(
            self._auth_endpoint, data=self.oauth_request_body, headers=headers
        )
        try:
            token_response.raise_for_status()
            self.logger.info("Access token was updated")
        except Exception as ex:
            self.state.update({"auth_error_response": token_response.text})
            raise RuntimeError(
                f"Failed auth, response was '{token_response.text}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]
        self._tap._config["access_token"] = token_json["access_token"]
        self._tap._config["expires_at"] = token_json["expires_at"]

        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)
